import * as _ from "lodash";
import { postCSV } from "../helpers/api-helper";
/**
 * Main controller function for POST /feed/trigger
 * @param {Object} req ExpressJS request object.
 * @param {Object} res ExpressJS response object
 */
async function postFeedTrigger(req, res) {
  const startTime = new Date();
  const request = await postCSV();

  // Logging here because we won't be calling from the shell
  console.log("[question-4-additional-service] request result: %o", request);

  res
    .status(200)
    .json({
      status: 200,
      msg: `Successfully called POST /sales/record within ${(
        (new Date() - startTime) /
        1000
      ).toFixed(3)} seconds`,
      timestamp: +new Date(),
    });
}

module.exports = {
  postFeedTrigger,
};
