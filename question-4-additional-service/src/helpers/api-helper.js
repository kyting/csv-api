import fs from "fs";
import * as _ from "lodash";
import request from "request-promise";

// Possible improvement: move all config for host and port to one place
const baseUri = process.env.CSV_BACKEND
  ? process.env.CSV_BACKEND
  : "http://localhost:8080";

async function postCSV(csv) {
  const options = {
    method: "POST",
    uri: baseUri + "/sales/record",
    formData: {
      name: "file",
      file: {
        value: fs.createReadStream(_.isNil(csv) ? "test.csv" : csv),
        options: {
          filename: "test.csv",
          contentType: "text/csv",
        },
      },
    },
  };
  return await request(options);
}

module.exports = {
  postCSV,
};
