import express from "express";
import { ping } from "./controllers/ping";
import { postFeedTrigger } from "./controllers/postFeedTrigger";

const router = express.Router();

// GET Endpoints
router.get("/", ping);
router.post("/feed/trigger", postFeedTrigger);

module.exports = {
  router,
};
