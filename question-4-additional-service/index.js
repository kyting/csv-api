import express from "express";
import bodyParser from "body-parser";
import { router } from "./src/router";


const port = process.env.ADDITIONAL_SERVICE_PORT ? process.env.ADDITIONAL_SERVICE_PORT : 8081 ;
const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(router);

app.listen(port, function () {
  console.log("Additional service started.");
});
