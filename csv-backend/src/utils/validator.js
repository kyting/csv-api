import * as _ from "lodash";
import { validate } from "validate.js";

/**
 * Validation constraints
 */
const constraints = {
  USER_NAME: {
    presence: true,
  },
  AGE: {
    presence: true,
    numericality: {
      onlyInteger: true,
      greaterThan: 0,
    },
  },
  HEIGHT: {
    presence: true,
    numericality: {
      onlyInteger: true,
      greaterThan: 0,
    },
  },
  GENDER: {
    presence: true,
    format: {
      pattern: "^(?:m|M|male|Male|f|F|female|Female)$",
    },
  },
  SALE_AMOUNT: {
    presence: true,
    numericality: {
      onlyInteger: false,
      greaterThan: 0,
    },
  },
  // NOTE 1: Moving validation for LAST_PURCHASE_DATE outside due to the following:
  // https://github.com/aurelia/validatejs/issues/96#issuecomment-227803895
  LAST_PURCHASE_DATE: {
    presence: true,
  },
};

/**
 * Validation function for CSV objects mapped by the mapStringToHeaders function
 * @param {Object} input Mapped CSV object
 * @returns {Object} Validation object with status and error
 */
function validateLine(input) {
  // First validation layer - check if all the fields exist
  const validationResults = validate(input, constraints);

  if (!_.isEmpty(validationResults)) {
    throw new Error(JSON.stringify(validationResults));
  }

  // Validate date using moment.js (see Note 1 above)
  if (!input.LAST_PURCHASE_DATE.isValid()) {
    throw new Error(
      JSON.stringify({ LAST_PURCHASE_DATE: ["LAST PURCHASE DATE is invalid"] })
    );
  }

  return true;
}

/**
 * Function to check if a string can be represented as a number
 * @param {string} n to be checked
 */
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

module.exports = {
  validateLine,
  isNumeric,
};
