import { Sale } from "../sequelize";

/**
 * Function for inserting DB record
 * @param {*} input
 */
async function insertSalesRecord(input) {
  return await Sale.create({
    userName: input.USER_NAME,
    age: input.AGE,
    height: input.HEIGHT,
    gender: input.GENDER.toUpperCase(),
    saleAmount: input.SALE_AMOUNT,
    lastPurchaseDate: input.LAST_PURCHASE_DATE.toISOString(),
  });
}

module.exports = {
  insertSalesRecord,
};
