import express from "express";
import bodyParser from "body-parser";

// Initialize app and router
const app = express();
const router = require("./router");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(router);

module.exports = app;
