const Sequelize = require("sequelize");
const SaleModel = require("./models/sale");

const sequelize = new Sequelize("postgres", "postgres", "postgres", {
  host: process.env.DOCKER_COMPOSE ? "postgres_db" : "localhost",
  port: "5432",
  dialect: "postgres",
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

const Sale = SaleModel(sequelize, Sequelize);

module.exports = {
  sequelize,
  Sale,
};
