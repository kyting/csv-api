import { triggerFeed } from "../helpers/api-helper";

/**
 * Sleep for a given number of seconds
 * @param {seconds} seconds Number of seconds to sleep
 */
function sleep(seconds) {
  const milliseconds = seconds * 1000;
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

async function callExternalAPIService() {
  // intentionally wants it to infinite loop
  while (true) {
    try {
      triggerFeed();
    } catch (err) {
      console.error(
        "Unable to trigger feed data due to the following error: %o",
        err
      );
    }
    // sleep for timer duration
    await sleep(60);
  }
}

module.exports = {
  callExternalAPIService,
};
