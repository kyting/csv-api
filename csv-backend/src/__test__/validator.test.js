import moment from "moment";
import { expect } from "chai";
import { validateLine } from "../utils/validator";

describe("validateLine tests", () => {
  const input = {
    USER_NAME: "John Doe",
    AGE: 1, // Very young
    HEIGHT: 20000, // But very big
    GENDER: "M",
    SALE_AMOUNT: 20,
    LAST_PURCHASE_DATE: moment(123),
  };

  it("should pass validation for a normal case", () => {
    expect(validateLine(input)).to.equal(true);
  });

  it("should pass validation for Regex in Gender field", () => {
    expect(validateLine({ ...input, GENDER: "M" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "m" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "male" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "Male" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "f" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "F" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "female" })).to.equal(true);
    expect(validateLine({ ...input, GENDER: "Female" })).to.equal(true);
  });

  it("should fail validation for invalid (uncaptured) regex", () => {
    try {
      validateLine({ ...input, GENDER: "ABCD" });
    } catch (err) {
      expect(err.message).to.equal(
        JSON.stringify({ GENDER: ["Gender is invalid"] })
      );
    }
  });

  it("should fail validation for an undefined user_name", () => {
    try {
      validateLine({ ...input, AGE: 1, HEIGHT: 2000 });
    } catch (err) {
      expect(err.message).to.equal(
        JSON.stringify({ USER_NAME: ["User name can't be blank"] })
      );
    }
  });

  it("should fail validation for an fail validation for invalid age and height", () => {
    try {
      validateLine({ ...input, AGE: "A", HEIGHT: -1 });
    } catch (err) {
      expect(err.message).to.equal(
        JSON.stringify({
          AGE: ["Age is not a number"],
          HEIGHT: ["Height must be greater than 0"],
        })
      );
    }
  });

  it("should fail validation for an negative sale amount", () => {
    try {
      validateLine({ ...input, SALE_AMOUNT: -1 });
    } catch (err) {
      expect(err.message).to.equal(
        JSON.stringify({ SALE_AMOUNT: ["Sale amount must be greater than 0"] })
      );
    }
  });

  it("should fail validation for an invalid moment.js date", () => {
    try {
      validateLine({ ...input, LAST_PURCHASE_DATE: moment.invalid() });
    } catch (err) {
      expect(err.message).to.equal(
        JSON.stringify({
          LAST_PURCHASE_DATE: ["LAST PURCHASE DATE is invalid"],
        })
      );
    }
  });
});
