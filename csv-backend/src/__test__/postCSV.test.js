import { expect } from "chai";
import dedent from "dedent";

import { postSalesRecord } from "../controllers/postSalesRecord";

describe("Test CSV validation implementation implementation", () => {
  it("should submit the provided CSV string successfully", async () => {
    // Use dedent to remove indentation
    const input = dedent(
      "USER_NAME,AGE,HEIGHT,GENDER,SALE_AMOUNT,LAST_PURCHASE_DATE\n\
        John Doe,29,177,M,21312.12,2020-11-05T13:15:30Z\n\
        Jane Doe,32,187,f,5342,2019-12-05T13:15:30+08:00"
    );

    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      successfulCount: 2,
      failedCount: 0,
      failedLines: [],
    });
  });

  it("Test 0: should pass in a valid string", async () => {
    const input = undefined;
    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      error: "VALIDATION ERROR: Invalid file format",
    });
  });

  it("Test 1: should not be able to submit an empty string", async () => {
    const input = "";
    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      error: "VALIDATION ERROR: File is empty",
    });
  });

  it("Test 2: should fail when incorrect headers are provided", async () => {
    const input = dedent(
      "userNAME,AGAAE,HEIASGHT,GENSSDER,SAALE_AMOUNT,LAAAST_PURCHASE_DATE\n\
        John Doe,29,177,M,21312.12,2020-11-05T13:15:30Z\n\
        Jane Doe,32,187,f,5342,2019-12-05T13:15:30+08:00"
    );

    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      error:
        "VALIDATION ERROR: Headers do not correspond to the header definition",
    });
  });

  it("Test 3: should not process if the headers are not provided", async () => {
    const input = dedent(
      "John Doe,29,177,M,21312.12,2020-11-05T13:15:30Z\n\
        Jane Doe,32,187,f,5342,2019-12-05T13:15:30+08:00"
    );

    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      error:
        "VALIDATION ERROR: Headers do not correspond to the header definition",
    });
  });

  it("should return unsuccessfully validated lines due to invalid input", async () => {
    // Hint: Jane Doe is an alien
    const input = dedent(
      "USER_NAME,AGE,HEIGHT,GENDER,SALE_AMOUNT,LAST_PURCHASE_DATE\n\
        John Doe,29,177,M,21312.12,2020-11-05T13:15:30Z\n\
        Jane Doe,32,187,ALIEN,5342,2019-12-05T13:15:30+08:00"
    );

    const response = await postSalesRecord.processStringInput(input, true);
    expect(response).to.deep.equal({
      successfulCount: 1,
      failedCount: 1,
      failedLines: ["Jane Doe,32,187,ALIEN,5342,2019-12-05T13:15:30+08:00"],
    });
  });
});
