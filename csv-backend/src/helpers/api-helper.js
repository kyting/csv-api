import request from "request-promise";

// Possible improvement: move all config for host and port to one place
const baseUri = process.env.ADDITIONAL_SERVICE
  ? process.env.ADDITIONAL_SERVICE
  : "http://localhost:8081";

async function triggerFeed() {
  const options = {
    method: "POST",
    uri: baseUri + "/feed/trigger",
  };

  const result = await request(options);
  return result;
}

module.exports = {
  triggerFeed,
};
