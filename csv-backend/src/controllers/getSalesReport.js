import * as _ from "lodash";
import moment from "moment";
import { isNumeric } from "../utils/validator";
import { Sale } from "../sequelize";

const { Op } = require("sequelize");

/**
 * Main implementation for querying data based on date
 * @param {string} fromDate String representing
 * @param {string} toDate
 */
async function querySalesRecord(fromDate, toDate) {
  // There are 3 cases:
  // Case 1: fromDate and toDate exist
  // Case 2: Only fromDate exists
  // Case 3: Both fromDate and toDate do not exist
  // Case 4 [INVALID]: Only toDate exists

  // Convert to number if not datestring.
  // There is a condition (bug?) where '100000' cannot be represented as a moment object
  // This can be verified using: moment.parseZone('100000').isValid()
  if (isNumeric(fromDate)) {
    fromDate = parseInt(fromDate);
  }
  if (isNumeric(toDate)) {
    toDate = parseInt(toDate);
  }

  let fromDateObject, toDateObject;

  if (fromDate) {
    fromDateObject = moment.parseZone(fromDate).isValid()
      ? moment.parseZone(fromDate)
      : moment.invalid();
  }

  if (toDate) {
    toDateObject = moment.parseZone(toDate).isValid()
      ? moment.parseZone(toDate)
      : moment.invalid();
  }

  // Time filter
  let whereParams = {};
  // Case 1: fromDate and toDate exist
  // Therefore both fromDateObject and toDateObject should both be valid. Otherwise, fail the request
  if (fromDate && toDate) {
    if (fromDateObject.isValid() && toDateObject.isValid()) {
      // Query and return
      whereParams = {
        lastPurchaseDate: {
          // $between: [fromDateObject, toDateObject],
          [Op.gte]: fromDateObject,
          [Op.lte]: toDateObject,
        },
      };
    } else {
      throw new Error(
        `Validation error: fromDate: ${
          fromDateObject.isValid() ? "valid" : "invalid"
        }, toDate: ${toDateObject.isValid() ? "valid" : "invalid"}`
      );
    }
  }

  // Case 2: fromDate exists, but toDate doesn't
  else if (fromDate && !toDate) {
    if (fromDateObject.isValid()) {
      whereParams = {
        lastPurchaseDate: {
          [Op.gte]: fromDateObject,
        },
      };
    } else {
      throw new Error(
        "Validation error: fromDate is passed in, but is invalid"
      );
    }
  }

  // Case 3: Both fromDate and toDate do not exist
  else if (!fromDate && !toDate) {
    whereParams = {};
  } else if (!fromDate && toDate) {
    throw new Error(
      "Invalid operation: fromDate is not passed in, but toDate is."
    );
  }

  try {
    const result = await Sale.findAll({ where: whereParams, raw: true });
    return result;
  } catch (err) {
    console.log("ERROR: %o", err);
  }
}

/**
 * Main controller function for GET /sales/report
 * @param {Object} req ExpressJS request object.
 * @param {string} req.query.fromDate From date filter
 * @param {string} req.query.toDate To date filter
 * @param {Object} res ExpressJS response object
 */
async function getSalesReportController(req, res) {
  let result;

  try {
    result = await querySalesRecord(req.query.fromDate, req.query.toDate);
  } catch (err) {
    console.log("ERROR: %o", err.message);
    res.status(400).json({
      status: 400,
      msg: "",
      error: err.message,
      timestamp: +new Date(),
    });
    return;
  }

  res.status(200).json({
    status: 200,
    msg: `Success!`,
    data: result,
    timestamp: +new Date(),
  });
}

module.exports = {
  getSalesReportController,
};
