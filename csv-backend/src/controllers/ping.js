/**
 * Main controller function for GET /
 * @param {Object} req ExpressJS request object.
 * @param {Object} res ExpressJS response object
 */
async function ping(req, res) {
  res
    .status(200)
    .json({ status: 200, msg: `Success!`, timestamp: +new Date() });
}

module.exports = {
  ping,
};
