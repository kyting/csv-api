import * as _ from "lodash";
import moment from "moment";
import { validateLine } from "../utils/validator";
import { insertSalesRecord } from "../utils/dbHelper";
import { headers } from "../primitives";

/**
 * Maps a CSV line to a Headers object
 * @param {string} input comma-delimited string
 * @returns {Object} Mapped Headers object
 */
function mapStringToHeaders(input) {
  const arr = input.split(",").map((el) => {
    const n = Number(el);
    return n === 0 ? n : n || el;
  });

  const result = headers.reduce((o, k, i) => ({ ...o, [k]: arr[i] }), {});

  if (!_.isNil(result.LAST_PURCHASE_DATE)) {
    // Convert to moment.js object
    // Save everything as UTC
    // We let the errors bubble to the validator if it can't be created
    result.LAST_PURCHASE_DATE = moment
      .parseZone(result.LAST_PURCHASE_DATE)
      .isValid()
      ? moment.parseZone(result.LAST_PURCHASE_DATE)
      : moment.invalid();
  }
  return result;
}

/**
 * Process entire CSV string in-memory
 * Can be further improved with fileStreams if there is time
 * @param {string} input Entire CSV in the form of a string
 * @returns {Object} Returns { succesfulCount: number, }
 */
export async function processStringInput(input, testMode = false) {
  let lines;

  // If the file cannot be split, it is not a string
  try {
    lines = input.split("\n");
  } catch (err) {
    return { error: "VALIDATION ERROR: Invalid file format" };
  }

  // Test #1: Check that the length is > 0
  if (lines.length === 1 && lines[0] === "") {
    return { error: "VALIDATION ERROR: File is empty" };
  }

  const csvHeaders = lines.shift().split(",");

  // Test #2: Check that the first line's headers are the same
  if (!_.isEqual(headers, csvHeaders)) {
    return {
      error:
        "VALIDATION ERROR: Headers do not correspond to the header definition",
    };
  }

  const processingResults = {
    successfulCount: 0,
    failedCount: 0,
    failedLines: Array(),
  };

  for (const line of lines) {
    try {
      const mapped = mapStringToHeaders(line);
      validateLine(mapped);
      if (!testMode) {
        insertSalesRecord(mapped);
      }
      processingResults.successfulCount += 1;
    } catch (err) {
      processingResults.failedCount += 1;
      processingResults.failedLines.push(line);
    }
  }
  return processingResults;
}

/**
 * Main controller function for POST /sales/record
 * @param {*} req ExpressJS request object
 * @param {*} res ExpressJS response object
 */
async function postSalesRecordController(req, res) {
  if (_.isNil(req.file)) {
    res.status(400).json({
      status: 400,
      msg: "",
      error: "No file was attached with the request",
      timestamp: +new Date(),
    });
    return;
  }

  let csvStr;
  try {
    csvStr = req.file.buffer.toString();
  } catch (err) {
    res.status(400).json({
      status: 400,
      msg: "",
      error: "Invalid file",
      timestamp: +new Date(),
    });
    return;
  }

  const result = await processStringInput(csvStr);

  // Return lines that cannot be stringified
  // Using json.parse(json.stringify(lines)) in order to remove newline characters and other delimiters
  if (result.failedCount > 0) {
    res.status(500).json({
      status: 500,
      msg: "",
      error: `Unable to process the following lines: ${JSON.parse(
        JSON.stringify(result.failedLines)
      )}`,
      timestamp: +new Date(),
    });
    return;
  }

  res.status(200).json({
    status: 200,
    msg: `Successfully saved ${result.successfulCount} lines.`,
    data: result,
    timestamp: +new Date(),
  });
}

module.exports = {
  postSalesRecordController,
  processStringInput,
};
