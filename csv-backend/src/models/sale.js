module.exports = (sequelize, type) => {
  return sequelize.define("sale", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userName: {
      type: type.STRING,
    },
    age: {
      type: type.INTEGER,
    },
    height: {
      type: type.INTEGER,
    },
    gender: {
      type: type.STRING,
    },
    saleAmount: {
      type: type.FLOAT,
    },
    lastPurchaseDate: type.DATE,
  });
};
