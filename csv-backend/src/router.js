import multer from "multer";
import express from "express";

import { ping } from "./controllers/ping";
import { postSalesRecordController } from "./controllers/postSalesRecord";
import { getSalesReportController } from "./controllers/getSalesReport";

const router = express.Router();

const upload = multer({
  storage: multer.memoryStorage(),
});

// GET Endpoints
router.get("/", ping);
router.get("/sales/report", getSalesReportController);

// POST Endpoints
router.post("/sales/record", upload.single("file"), postSalesRecordController);

module.exports = router;
