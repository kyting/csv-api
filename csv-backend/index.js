import app from "./src/app";
import { sequelize } from "./src/sequelize";
import { callExternalAPIService } from "./src/services/externalAPIService";

// Initialize DB connection
sequelize.sync({ force: false }).then(() => {
  console.log(`Database & tables initialized!`);
});

const main = async () => {
  const port = process.env.CSV_BACKEND_PORT ? process.env.CSV_BACKEND_PORT : 8080;
  app.listen(port);
  if (process.env.ADDITIONAL_SERVICE) {
    callExternalAPIService();
  }
};

main();
