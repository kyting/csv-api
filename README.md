## Background
This is a submission to the Coding Test. 

The backend currently comprises of the following components:

* `csv-backend` service (Express)
* `question-4-additional-service` service (Express)
* Postgres DB

Please note that everything is in a monorepo configuration to demonstrate the Docker-Compose capability. Both Express backends are may contain similar code. However, in a proper microservice configuration, it is possible for them to have their own stack with pre-defined interfaces.

## Setup
This application has already been Dockerized in this merge request: https://gitlab.com/kyting/csv-api/-/merge_requests/1

We are using docker-compose to spin up multiple services on the same machine in one command. To set up, run the following:

```bash
docker-compose up --build -d
```

This will spin up the csv-backend, question-4-additional-service, and Postgres container on ports 8080, 8081, and 5432 respectively. The `-d` flag sends the process to the background.

Upon starting, `csv-backend` will call `question-4-additional-service` every 60 seconds for demonstration purposes, rather than every hour in the assignment instructions. It will upload the `question-4-additional-service/test.csv` file to `csv-backend`.

Further development can involve GitLab CI/CD to enable one-click-deploy, as well as a Kubernetes cluster to ensure uptime.

## API Definition
The `csv-backend` service contains three APIs defined as follows, defined in `src/router.js`:

* `GET /` - Check if the server is alive
* `POST /sales/record` - Post a CSV file to the server, which will be added to the DB, and will be query-able
* `GET /sales/report` - Query record that have been posted to the server, and returns an array of these records.

The app's entry point is `index.js` in the root folder and currently runs on port 8080 by default - future work can be done to make it configurable.

The rest of this section discusses the behaviour of the endpoints.

#### GET /
The `GET /` endpoint is just a health check, which can be used for pinging the server to see if it is alive:

```bash
curl -X GET 'http://localhost:8080/'
```


#### POST /sales/record
In order to post a CSV file to the DB, assuming you are in the root directory, the following cURL command can be used:

```bash
curl --form "file=@csv-backend/test.csv" -X POST http://localhost:8080/sales/record
```

A `test.csv` file has been added to the repository for your convenience

This then sets the following into motion:

1. The current solution uses Express as a backend and uses the Multer middleware to allow single file uploads. 
2. Multer allows files to be stored in-memory in a buffer, which is then converted into a string.
3. The string is then sent for validation line-by-line, starting with the headers, and followed by the data
4. After the validation of each line, it is saved in the DB using Sequelize, an ORM. At the moment there is no check for duplicates, so a new entry will just be appended to the DB
5. The endpoint then returns the number of successful lines that have been posted. If there are any failed lines, these are returned in the response


#### GET /sales/record

There are several ways to query this endpoint. There are 2 parameters that can be passed, `toDate` and `fromDate`. Both are strings or epochs, which will be converted by the `moment.js` library into a `moment` object. Thus, the parameters can passed in either in epoch time or a datestring, and the `moment` parser will handle the conversion.


##### Option 1: Both fromDate and toDate exist

```bash
# Both date formats are valid
curl -X GET 'http://localhost:8080/sales/report?fromDate=1575522800000&toDate=2020-12-25T12:00:30Z'
```

##### Option 2: Only fromDate exists

```bash
# Query for everything on or after this date
curl -X GET 'http://localhost:8080/sales/report?fromDate=2020-11-05T13:15:30.000Z'
```

##### Option 3: Both fromDate and toDate do not exist
```bash
# Query everything
curl -X GET 'http://localhost:8080/sales/report
```
##### Fail case
If `toDate` is passed, but `fromDate` is not, an error will be thrown:

```bash
# Throw error
curl -X GET 'http://localhost:8080/sales/report?toDate=2020-11-05T13:15:30.000Z'
```

### Additional Service Repo
The `question-4-additional-service` service contains two APIs defined as follows, defined in `src/router.js`:

* `GET /` - Check if the server is alive
* `POST /feed/trigger` - Triggers a request to call `csv-service`'s `POST /sales/record` endpoint

## Tests

At the moment, tests only cover the `csv-backend` repo. Tests currently cover implementation functions (unit tests), However, there has been insufficient time to implement end-to-end tests.

Tests are run using Jest, and are found in the `src/__tests__` folder. Tests have the `*.test.js` extension.


### Test Instructions
Step 1: `cd` into the `csv-backend` folder.

```bash
cd csv-backend
```

Step 2: Ensure dependencies are installed.
```bash
yarn install
```

Step 3: Run the test
```bash
yarn test
```

## Future Work
This section shows items which were not implemented due to lack of time constraints:

- I would have liked the files to be less "hacky" (make everything more configurable), and more clean
- More test cases to cover end-to-end on both backend services
- Add swagger definitions and a swagger service
- Custom test environment in test cases
- Add CI/CD integration
- Mock test data on every triggered data feed

## Bonus points

- [x] **Use any popular server framework** - Done. Express is used here
- [x] **Include unit tests and end-to-end tests to cover different use cases** - Partially done, unit tests cover most things, but end-to-end test is not implemented
- [x] **You can assume the CSV data file is a very big file. e.g. 1GB or bigger** - Not tested, but should be covered by Multer, provided storage is enough (https://stackoverflow.com/q/51587422)
- [x] **Implement data storage with in-memory or actual database** - Done. Postgres is used here with the Sequelize ORM
- [x] **Additional service to trigger data feed in. Assuming there is an external endpoint that this service will call every hour, the external server will then call the /sales/record API with CSV data within 15s.** - Done.
- [x] **Deployment ready with tools. e.g. Docker setup** - Done.